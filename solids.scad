/*

Name:        Solids
Description: Simple geometrical solids for use in other projects
Version:     v0.2
Creator:     Simon Wood
Creator URI: https://simonwood.info
License:     CC-NC-BY-SA 4.0
License URI: https://creativecommons.org/licenses/by-nc-sa/4.0/
*/



/*
 * A tube
 */
module tube(height,outer_radius,inner_radius,side_taper=0,end_taper=0) {
    difference(){
        tapered_side_radius=outer_radius-side_taper*height/100;
        tapered_end_height=tapered_side_radius*end_taper/100;
        union(){
            cylinder(height,outer_radius,tapered_side_radius);
            translate([0,0,height])
                cylinder(tapered_end_height,tapered_side_radius,0);
        }
        translate([0,0,-1])
            cylinder(height+2+tapered_end_height,inner_radius,inner_radius);
    }
}

/*
 * A part of a tube defined by an angle
 */
module tube_sector(height,outer_radius,inner_radius,angle){
    intersection(){
        tube(height,outer_radius,inner_radius);
        rotate(angle/2)
            translate([0,-outer_radius,0])
                cube([outer_radius,2*outer_radius,height]);
        rotate(90-angle/2)
            translate([-outer_radius,0,0])
                cube([2*outer_radius,outer_radius,height]);
    }    
}

/*
 * A torus (set an angle of less than 360 for an incomplete torus)
 */
module torus(radius,circle_radius,angle=360) {
    rotate_extrude(angle = angle)
        translate([radius,0,0])
            circle(r = circle_radius);
}

/*
 * Cuboid with four parallel edges rounded (or two if semi=true)
 */
module extruded_round_cornered_rectangle(length,width,corner_radius,thickness,semi=false) {
    linear_extrude(height=thickness) {
        round_cornered_rectangle(length,width,corner_radius,semi);
    }
}

/*
 * Rectangle with all four the corners rounded off (or two if semi=true)
 */
module round_cornered_rectangle(length,width,corner_radius,semi=false) {
    hull() {
        for(i=[0:1],j=[0:1]) {
            translate([corner_radius*(-1)^i+length*i,corner_radius*(-1)^j+width*j,0])
                circle(corner_radius);
        }
        if(semi)
            square([corner_radius,width]);
    }
}

/*
 * Part of a tube, 
 * defined by the length of the chord separating a segment
 */
module cylinder_segment(height,radius,chord_length=0,angle=0){
    a = (chord_length!=0) ? angle_subtended_by_chord(chord_length,radius) : angle;
    translate([0,-radius*cos(a/2),0])
        rotate(180)
            difference() {
                cylinder(h=height,r=radius);
                translate([-radius-1,-radius*cos(a/2),-1])
                    cube(size=[2*radius+2,2*radius+2,height+2]);
            }
}

/*
 * A hollow sphere
 */
module hollow_sphere(outer_radius,inner_radius) {
    difference() {
        sphere(r=outer_radius);
        sphere(r=inner_radius);
    }    
}

/*
 * Quarter of a hollow sphere
 */
module quarter_hollow_sphere(outer_radius,inner_radius) {
    intersection() {
        hollow_sphere(outer_radius,inner_radius);
        translate([-outer_radius,0,0])
        cube(size=[2*outer_radius,outer_radius,outer_radius]);
    }
}

/*
 * Calculate the angle subtended by a chord 
 * in a circle of a given radius
 */
function angle_subtended_by_chord(chord_length,circle_radius) = 2*asin(0.5*chord_length/circle_radius);


/*
 * Rectangular frame (i.e. can be used as simple window frame etc.)
 */
module frame(length,width,height,frame_size) {
    difference() {
        cube(size=[length,width,height]);
        translate([frame_size,frame_size,-1])
            cube(size=[length-2*frame_size,width-2*frame_size,height+2]);
    }
}